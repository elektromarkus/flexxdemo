# flexxdemo

This repository contains a few examples of the flexx Python framework from Almar Klein.

Homepage: https://flexx.app/

Docs: https://flexx.readthedocs.io/

Source: https://github.com/flexxui/flexx


The following examples are available:

**Hello World**

The classic!

**Button**

A simple example of using the event queue.

**Complex**

The following components are shown in this example:
1. Use of the event queue from the back end via the session management to the front end
2. Use of the Tornado Web Server as a backend REST service

**Google Maps**

Here it is shown how the Javascript based Google Map API is integrated into flexx. The following events are involved:
1. center_change
2. click
3. drag
4. bounds_changed

