# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 06:42:36 2019

@author: markus
Aufwand ca. 3,5 Stunden
"""

import psutil
import asyncio
import tornado.web
from pprint import pprint
import json

from flexx import flx


# das ist eine serverseitige Komponente
# pure Python keine javascript
class Relay(flx.Component):
    timerdelay = 1

    def init(self):
        self.refresh()

    # der emitter sendet async events zur Application
    # echt klasse event-Feature auf basis von Websockets
    @flx.emitter
    def system_info(self):
        return dict(cpu=psutil.cpu_percent(), mem=psutil.virtual_memory().percent)

    # das ist die einbettung in das eventloop system von python
    def refresh(self):
        self.system_info()
        asyncio.get_event_loop().call_later(self.timerdelay, self.refresh)


# hier dann die Instanz zur obigen Komponente
relay = Relay()


# das ist auch eine serverseitige Komponente
# auch hier pure python,
# sie unterscheidet sich von der obigen, dass sie sessionbezogen instantiiert wird
class Monitor(flx.PyComponent):

    def init(self):
        with flx.VBox():
            self.monitorView = MonitorView()

    # dies ist der Empfaenger des obigen emmitters
    # jede session bekommt einen eigenen callback
    # im Sinne eines MVC ist dies der Controller
    @relay.reaction('system_info')
    def _push_info(self, *events):
        if not self.session.status:
            return relay.disconnect('system_info:' + self.id)
        for ev in events:
            # hier wird der event vom BE an das FE gesendet
            self.monitorView.updateInfo(dict(cpu=ev.cpu, mem=ev.mem))


# das hier ist die client seitige componenten
class MonitorView(flx.VBox):
    def init(self):
        # self.label = flx.Label(text="Arena 2")
        self.info = flx.Label(text="")
        flx.VBox()

    # die action fuehrt die reacton aus
    @flx.action
    def updateInfo(self, info):
        self.info.set_text('CPU:' + str(info.cpu) + " Memory:" + str(info.mem))


# hier ist der  View
class Combine(flx.PyComponent):
    def init(self):
        with flx.VBox():
            self.arena1 = EnterView()
            self.arena2 = Monitor()
            flx.VBox()


# das ist der EnterView
class EnterView(flx.VBox):
    CSS = """
    .flx-EnterView {
        background: red;
        border: 10px solid #000;
    }
    """

    def init(self):
        # init des REST-Clients
        self.dataHandler = XHRHttpReqestHandler()

        # hole einen ersten Datensatz vom Backende
        self.dataHandler.requestPath("/service/data/0")

        # initialisiere die GUI
        with flx.VBox():
            self.selector = flx.ComboBox(editable=False, options=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
            self.name = flx.Label()
            self.adr = flx.LineEdit()
            flx.VBox()

    # der event handler reagiert, wenn der Anwender in der Combobox ein Feld ausgewaehlt hat
    @flx.reaction('selector.user_selected')
    def _selector(self, *event):
        for ev in event:
            self.index = ev.index

        #  REST-Anfrage an das backend, die Response wird nicht abgewartet sondern wird in einer Reaction verarbeitet
        self.dataHandler.requestPath("/service/data/" + self.index)

    # hier geht's los, wenn der user ein return im eingabefeld gemacht hat
    @flx.reaction('adr.submit')
    def _enter(self, *event):
        # hier wird das BE aufgerufen. Die Antwort landet in der reaction unterhalb...
        result = self.dataHandler.postRequestPath('/service/data/' + self.index, {'adr': self.adr.text})

    # hier kommen die daten async zum obigen request an
    @flx.reaction('dataHandler.reqListener')
    def _react(self, *events):
        # die Variable "window" ermöglicht den Zugriff auf weitere javascript funktionten
        global window
        for ev in events: # eigentlich wird nur das erste Element der event-liste gebraucht
            j = window.JSON.parse(ev.text)
            name = j['name']
            adr = j['adr']
            # setzen der Widget properties
            self.name.set_text(name)
            self.adr.set_text(adr)


# request Handler des Frontendes
class XHRHttpReqestHandler(flx.Widget):
    host = flx.StringProp('localhost', settable=True)
    port = flx.StringProp('49190', settable=True)
    protokoll = flx.StringProp('http', settable=True)

    @flx.emitter
    def reqListener(self, *event):
        self.reText = ""
        for ev in event:
            self.reText = ev.currentTarget.responseText
        if self.reText != "":
            return dict(text=self.reText)

    def init(self):
        global window
        self.baseUrl = self.protokoll + '://' + self.host + ':' + self.port
        self.baseUrl = ""
        print("XHR Base Url:" + self.baseUrl)
        self.xhrRequest = window.XMLHttpRequest()
        # self.xhrRequest.responseText = self.reqListener
        self.xhrRequest.addEventListener("load", self.reqListener)

    def requestPath(self, path):
        reqString = self.baseUrl + path
        print("Request:" + reqString)
        self.xhrRequest.open("GET", reqString)
        self.xhrRequest.send()

    def postRequestPath(self, path, data):
        reqString = self.baseUrl + path
        print(reqString)
        s = window.JSON.stringify(data)
        self.xhrRequest.open("POST", reqString)
        self.xhrRequest.setRequestHeader('Content-type', 'json')
        self.xhrRequest.send(s)


# server BackEnd für GET und POST
class DataHandler(tornado.web.RequestHandler):
    def initialize(self, data):
        self.data = data

    def get(self, numStr):
        num = int(numStr)
        if num < len(self.data):
            # Die Client Anfrage wird beantwortet
            self.write(self.data[num])
        else:
            print(f"Num:{num} not available")
            self.write({'name': '', 'adr': ''})

    def post(self, path):
        changeValue = json.loads(self.request.body)
        # der auf dem Server gespeicherte Datensatz wird per REST Post geaendert
        self.data[int(path)]['adr'] = changeValue['adr']
        pprint(self.data)


if __name__ == '__main__':
    # Init des Web Servers
    flx.create_server(host="0.0.0.0", port=49190)
    tornado_app = flx.current_server().app

    # Erweitern des App Webservers um einen REST Service
    tornado_app.add_handlers(r".*", [
        (r"/service/data/(.*)", DataHandler,
         dict(data=[{'name': 'markus', 'adr': "Zu hause"}, {'name': 'paul', 'adr': 'jwd'}])),
    ])

    # Instantiieren der drei Web Seiten
    flx.serve(EnterView, 'Arena_1')
    flx.serve(Monitor, 'Arena_2')
    flx.serve(Combine, 'Arena_3')

    # Start aller Services (REST und Pages)
    flx.start()
