from flexx import flx


class MyButton(flx.Widget):
    def init(self):
        flx.Label(text='ButtonTest')
        self.button1 = flx.Button(text="Run")

    @flx.reaction('button1.pointer_down')
    def _clickbutton1(self, *event):
        print("Button 1 hit")
        for ev in event:
            print(ev.type,ev.source.text)


flx.serve(MyButton, "")
flx.start()
