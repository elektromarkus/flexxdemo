#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  9 17:27:58 2019

@author: markus
"""


from flexx import flx, ui, app, event
from urllib.request import urlopen, Request

_GOOGLEAPIKEY = "AIzaSyBr1izlkq_tad337DyeR8yikNq7aYpkf_A"  #  please insert your own api key here
_googleMapsUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBr1izlkq_tad337DyeR8yikNq7aYpkf_A&libraries=visualization"


def _get_code(item):
    url = _googleMapsUrl
    req = Request(url, headers={'User-Agent': 'flexx/%s' % flx.__version__})
    return urlopen(req).read().decode()


class GMap(flx.Widget):

    mapstyle = flx.StringProp('width:1900px;height:1111px;flex=1;', settable=True)
    center = flx.DictProp(
        { 'lat': 50.950408, 'lng':6.912837},  # Chaos Computer Club e.V.
        settable=True)
    zoom = flx.IntProp(8, settable=True)

    def init(self):
        global window
        global document
        self.afterInit = True
        # print("Google Maps init")
        self.ready = False
        self.markerList = []

        fullsettings = {'center': self.center,
                        'zoom': self.zoom,
                        'overviewMapControl': True,
                        'mapTypeControl': True,
                        'mapTypeControlOptions': {
                            'style': window.google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                            'position': window.google.maps.ControlPosition.TOP_CENTER
                        },
                        'zoomControl': True,
                        'zoomControlOptions': {
                            'position': window.google.maps.ControlPosition.LEFT_CENTER
                        },
                        'scaleControl': True,
                        'streetViewControl': True,
                        'streetViewControlOptions': {
                            'position': window.google.maps.ControlPosition.LEFT_TOP
                        },
                        'fullscreenControl': True
                        }

        self.map = window.google.maps.Map(self.mapnode, fullsettings)
        if self.mapControl != None:
            self.map.controls[window.google.maps.ControlPosition.BOTTOM_LEFT].push(self.mapControl.node)

        self.map.addListener('center_changed', self.center_changed)
        self.map.addListener('click', self.click)
        self.map.addListener('drag', self.drag)
        self.map.addListener('bounds_changed', self.bounds_changed)

    def runsAfterInit(self):
        # helper function for async loading of google api
        if self.afterInit:
            # print("Runs after init only once")
            self.mapnode.setAttribute("style", self.mapstyle)
            self.afterInit = False
            self.ready = True
        else:
            print("This should never appeare")

    def _create_dom(self):
        # print('GoogleMap Createdom')
        global document
        node = document.createElement('div')
        self.mapnode = document.createElement('div')
        node.appendChild(self.mapnode)
        self.mapnode.id = 'mapContainer'
        return node

    @flx.emitter
    def drag(self):
        print("Drag")
        # print(e)
        return dict(bounds=self.map.getBounds(), center=self.map.getCenter())

    @flx.emitter
    def bounds_changed(self):
        print("Bounds changed")
        if self.afterInit:
            self.runsAfterInit()

        bounds = self.map.getBounds()
        center = self.map.getCenter()
        sw = bounds.getSouthWest()
        ne = bounds.getNorthEast()
        swlat = sw.lat()
        swlng = sw.lng()
        nelat = ne.lat()
        nelng = ne.lng()
        spanlat = min(ne.lat() - sw.lat(), 2) / 2
        spanlng = min(ne.lng() - sw.lng(), 2) / 2
        dlat = 0.3 * spanlat
        dlng = 0.3 * spanlng
        return dict(swlat=swlat, swlng=swlng, nelat=nelat, nelng=nelng)

    @flx.emitter
    def center_changed(self, e):
        print("center_changed")
        return dict(center=self.map.getCenter())

    @flx.emitter
    def click(self, e):
        print("Click")
        # latlngs = [[50,51,52,53],[4,5,6,7]]
        # self.polyLine(latlngs)
        lat = e.latLng.lat()
        lng = e.latLng.lng()
        self.createMarker(lat, lng)
        return dict(latLng=e.latLng)

    def addToMarkerList(self, marker, t):
        markerElement = {'marker': marker, 'type': t}
        self.markerList.append(markerElement)

    def createMarker(self, lat, lng):
        # myLatLng = {lat: -25.363, lng: 131.044};
        marker = window.google.maps.Marker(
            {'position': {'lat': lat, 'lng': lng},
             'map': self.map})
        self.addToMarkerList(marker, "marker")
        return marker


class SimpleGMap(flx.PyWidget):
    def init(self):
        with flx.VBox():
            with flx.HBox():
                self.gmap = GMap()


if __name__ == '__main__':
    print("Loading scrips fromm google")
    flx.assets.associate_asset(__name__, 'googlemaps.js', lambda: _get_code(''))
    flx.serve(SimpleGMap, "")
    flx.start()
